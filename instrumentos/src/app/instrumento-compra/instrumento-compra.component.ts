import { Component, Input, OnInit, HostBinding } from '@angular/core';
import { InstrumentoCompra } from '../models/instrumento-compra.component';

@Component({
  selector: 'app-instrumento-compra',
  templateUrl: './instrumento-compra.component.html',
  styleUrls: ['./instrumento-compra.component.css']
})
export class InstrumentoCompraComponent implements OnInit {
  @Input() instrumento: InstrumentoCompra;
  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() { 
    this.instrumento = new InstrumentoCompra("","");
  }

  ngOnInit(): void {
  }

}
