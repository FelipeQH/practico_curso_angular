import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstrumentoCompraComponent } from './instrumento-compra.component';

describe('InstrumentoCompraComponent', () => {
  let component: InstrumentoCompraComponent;
  let fixture: ComponentFixture<InstrumentoCompraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstrumentoCompraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstrumentoCompraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
