import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { InstrumentoCompraComponent } from './instrumento-compra/instrumento-compra.component';
import { ListaInstrumentosComponent } from './lista-instrumentos/lista-instrumentos.component';

@NgModule({
  declarations: [
    AppComponent,
    InstrumentoCompraComponent,
    ListaInstrumentosComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
