import { Component, OnInit } from '@angular/core';
import { InstrumentoCompra } from '../models/instrumento-compra.component';

@Component({
  selector: 'app-lista-instrumentos',
  templateUrl: './lista-instrumentos.component.html',
  styleUrls: ['./lista-instrumentos.component.css']
})
export class ListaInstrumentosComponent implements OnInit {
  instrumentos: InstrumentoCompra[];
  constructor() {
    this.instrumentos = [];
  }

  ngOnInit(): void {
  }

  guardar(tipo:string, marca:string):boolean{
    this.instrumentos.push(new InstrumentoCompra(tipo,marca));
    return false;
  }

}
